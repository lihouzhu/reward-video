# 打赏视频

#### Description
课程付费开发的全开源知识付费系统。功能包含直播课堂、付费视频、付费音频、付费阅读、会员系统、课程分销、课程拼团、直播带货、直播打赏、商城系统等。能够快速积学员、学员数据分析、智能转化客户、有效提高销售、吸引流量、网络营销、品牌推广的一款应用，且更适合企业二次开发

#### Software Architecture
Software architecture description

#### Installation

1.  xxxx
2.  xxxx
3.  xxxx

#### Instructions

1.  xxxx
2.  xxxx
3.  xxxx

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request


#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
